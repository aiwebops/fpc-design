% simple function to add all subdirectories to the path variable
function addSubDirs()
    folder = fileparts(mfilename('fullpath'));
    addpath(genpath(folder));
end