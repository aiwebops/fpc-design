% set default figure position and size
%set(groot,'defaultfigureposition',[400 250 900 750])

% declare global parameters
global a_main; % desired object distance in mm to focus on
global b_main; % focus distance from aperture into cam
global f_main; % main lens focal length in mm
global d_main % aperture diameter in mm

global a_ML; % distance between virtual image and MLA in mm
global b_ML; % distance between sensor and MLA in mm
global f_ML; % ML focal length in mm
global d_ML; % microlens diameter in mm

global m; % magnification between MLA and sensor plane

global lambda; % desired disparity coefficient
global DoF; % minimum and maximum distance of scene points (measured from 
            % main lens aperture) to be imaged into a single pixel

global pixel_size; % pixel size in mm
global sensor_height; % sensor height in mm

global sensor_response_func; % sensor response dependent on light incident angle
sensor_response_func = @(angle) calc_sensor_response(angle, 1.4, 1);
global sensor_max_angle; % maximum incident light angle for pixel to have a 
                         % significant response
                         
global sensor_response_values; % precalculated sensor responses for 
                               % integral approximation