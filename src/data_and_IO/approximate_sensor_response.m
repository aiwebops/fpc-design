%
% \brief approximate_sensor_response returns an approximate sensor pixel 
%                   response with respect to the incident angle of the 
%                   incoming light. It is approximate since the nearest
%                   pre-calculated value is returned.
%
% \param angle      incident angle of the incoming light
% \param in_radians true for angle in radians, false for degrees
% 
% \return p      	precalculated sensor response
%
function p = approximate_sensor_response(angle, in_radians)
    if nargin < 2
        in_radians = true;
    end
    
    global sensor_response_values;
    
    if in_radians
        p = sensor_response_values(451 + round(450 * angle * 4 / pi));
    else
        p = sensor_response_values(451 + round(10 * angle));
    end

end