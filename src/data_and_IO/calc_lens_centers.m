%
% \brief calc_lens_centers calculates the aperture position and lens 
%                       centers based on the lens data
%
% \param radius         of the spherical lens surfaces in mm
% \param thickness      of the optical elements in mm
% \param material       names as strings
% \param sa             semi apertures of the lens surfaces in mm
%
function calc_lens_centers(radius, thickness, material, sa)
    
    global position aperture_index d_main f_main;
    
    % calculate index of aperture surface in surface list
    aperture_index = -1;
    [num_surfaces, ~] = size(radius);
    for i = 1:num_surfaces-1
        if strcmpi(material{i},'air') && strcmpi(material{i+1},'air')
            aperture_index = i+1;
            break
        end
    end
    
    % calculate aperture and lens center positions
    if aperture_index ~= -1
        aperture_position = sum(thickness(1:aperture_index-1));
    else
        if radius(1) >= 0.0
            aperture_position = -0.001;
        else
            r = radius(1);
            h = sa(1);
            aperture_position = min(-0.001, 1.1 * (r + sqrt(r*r - h*h)));
        end
    end

    for i = 1:num_surfaces
        position(i) = radius(i) - aperture_position;
        if i > 1
            position(i) = position(i) + sum(thickness(1:i-1));
        end
    end
    
    f_main = sum(thickness(aperture_index:num_surfaces));
    d_main = 2 * sa(aperture_index);
    
    global sensor_max_angle sensor_response_func;
    
    % calculate maximum incident light angle for pixel to have a significant
    % response (more than 1% of 0 degree incidence response)
    response_at_0 = sensor_response_func(0);
    current_response = response_at_0;
    sensor_max_angle = 0;
    while (current_response/response_at_0 > 0.01)
        sensor_max_angle = sensor_max_angle + 0.1 * pi / 180;
        current_response = sensor_response_func(sensor_max_angle);
    end
    sensor_max_angle = ceil(sensor_max_angle * 180 / pi);
    
    % precalculate sensor responses
    global sensor_response_values;
    sensor_response_values = arrayfun(@(angle) calc_sensor_response( ...
        angle,1.4,1),linspace(-pi/4, pi/4, 901)); %0.1 degree steps
end