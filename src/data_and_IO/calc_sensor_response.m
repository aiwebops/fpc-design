%
% \brief calc_sensor_response calculates the sensor pixel response with
%                   respect to the incident angle of the incoming light.
%                   This calculation is based on the model used in the
%                   Blender add-on, i.e. an orthographic camera combined
%                   with a diffusor plane
%
% \param angle      incident angle of the incoming light in radians
% \param IOR        of the diffusor plane
% \param roughness	of the diffusor plane
% 
% \return p      	sensor response
%
function p = calc_sensor_response(angle, IOR, roughness)
    % calculate the microfacet angle required to result in the given
    % refraction angle
    if angle == 0
        angle_mf = 0;
    else
        % compute beta for the given alpha (angle of refraction)
        angle_mf = angle + atan(sin(angle) / (IOR - cos(angle)));
    end
    
    % calculate Beckmann distribution value
    expo = -(tan(angle_mf)^2)/(roughness^2);
    D = ((cos(angle_mf) > 0) / (pi * roughness^2 * cos(angle_mf)^4)) * exp(1)^expo;
    
    % calculate probability density value for sampling angle_mf
    % ggf "* 2.0 * pi * sin(angle_mf)" einfügen
    p = (D * cos(angle_mf)) / abs(1 - (cos(angle_mf)/(IOR * sqrt(1-((sin(angle_mf)^2) / IOR^2)))));
    p = norm(p);
end