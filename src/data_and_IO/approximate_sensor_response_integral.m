%
% \brief approximate_sensor_response returns an approximate sensor pixel 
%                   response integral based on the pre-calculated values
%
% \param angle_1    integral start
% \param angle_2    integral end
% 
% \return A      	integral of precalculated sensor responses
%
function A = approximate_sensor_response_integral(angle_1, angle_2)

    global sensor_response_values;
    
    start_idx = 451 + round(10 * min(angle_1,angle_2));
    end_idx = 451 + round(10 * max(angle_1,angle_2));
    
    A = sum(sensor_response_values(start_idx:end_idx) + ...
        sensor_response_values(start_idx+1:end_idx+1)) * 0.1 * pi / 360;

end