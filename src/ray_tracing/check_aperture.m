%
% \brief check_aperture checks whether a given ray is blocked by the main
%                   lens aperture
%
% \param p          ray point
% \param v          ray direction
% \param d_main 	aperture diameter in mm
%
% \return           true if blocked, false otherwise
%
function blocked = check_aperture(p, v, d_main)
    blocked = (abs(p(2)-p(1)*v(2)/v(1)) > d_main/2);
end
