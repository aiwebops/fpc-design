%
% \brief calc_optimum_d_ML_rt calculates the optimum size of d_ML 
%
% \param sensor_response_func   function to determine angular sensor
%                               response
% \param magnification         	factor between MLA and sensor
% \param debug_visualization    enables/disables plotting of the rays
%
% \return d_ML                  optimal microlens diameter in mm
% \return d_MLI_in_px           the MLI diameter in pixel
%
function [d_ML, d_MLI_in_px] = calc_optimum_d_ML_rt(sensor_response_func, ...
    magnification, debug_visualization)

    if nargin < 3
        debug_visualization = false;
    end
    
    global b_main a_ML b_ML allowed_MLI_overlap pixel_size;
    d_objective_sensor = b_main + a_ML + b_ML;
    
    % trace rays from sensor through MLA and objective to find minimum angle of
    % incident light for these sensor pixels
    sensor_position = [d_objective_sensor; 0];
    collected_min_rays = cell(1000,1);
    counter = 1;

    % calculate maximum incident light angle for pixel to have a significant
    % response (more than 1% of 0 degree incidence response)
    response_at_0 = sensor_response_func(0);
    current_response = response_at_0;
    max_angle = 0;
    while (current_response/response_at_0 > 0.01)
        max_angle = max_angle + 0.1 * pi / 180;
        current_response = sensor_response_func(max_angle);
    end
    max_angle = ceil(max_angle * 180 / pi);

    % calculate min and max direction for several pixel positions from which
    % they receive light
    for step_size = 2:5
        collects_light = true;
        within_magnification = true;
        while collects_light && within_magnification
            current_position = sensor_position + [0; 0.1^step_size];
            for j = 0:10*max_angle
                direction = [cos(pi + j*pi/1800); sin(pi + j*pi/1800)];
                [x,y,w,blocked] = trace_through_two_plane_MLA(...
                    current_position, direction, false, 2);
                if blocked
                    continue
                end
                [x_main,y_main,~,blocked] = trace_through_objective(...
                    [x(end);y(end)], w, false);
                collects_light = ~blocked;

                if collects_light

                    % check if magnification factor holds
                    if (y_main(1) < 0) && (current_position(2) / ...
                            abs(y_main(1)) < magnification)
                        within_magnification = false;
                        break;
                    else
                        collected_min_rays{counter} = [[x(1:2);x_main],...
                        [y(1:2);y_main]];
                        counter = counter + 1;
                        sensor_position = current_position;
                    end

                    break
                end
            end
        end
    end

    % visualization for debugging 
    if debug_visualization
        figure;
        title("Tracing from sensor through objective for ML diameter calculation");
        visualize_rays(collected_min_rays);
    end

    % calculate ML diameter via 2 * maximum sensor position that was
    % successfully traced
    d_ML = (1 - allowed_MLI_overlap) * 2 * sensor_position(2);
    d_MLI = d_ML * magnification;
    d_MLI_in_px = d_MLI / pixel_size;
end