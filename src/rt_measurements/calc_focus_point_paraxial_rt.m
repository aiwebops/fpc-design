%
% \brief calc_focus_point_paraxial_rt calculates the paraxial approximation 
%                               for b_main
%
% \param a_main                 distance between scene point and main lens
% \param debug_visualization    enables/disables visualization
%
% \return b_main                paraxial approximation for b_main 
% \return ray_points            positions of traced ray intersections
% \return ray_directions        directions of the rays at these positions
%
function [b_main, ray_points, ray_directions] = calc_focus_point_paraxial_rt(...
    a_main, debug_visualization)

    if nargin < 2
        debug_visualization = false;
    end

    global d_main f_main position radius sa;

    % set small aperture for paraxial tracing
    temp_aperture = d_main;
    d_main = 1;

    % set ray starting position
    p = [-a_main; 0];

    % calculate maximum ray angle with respect to the x-axis that is not 
    % blocked by objective
    max_direction = [position(1) - radius(1) + a_main; sa(1)];
    max_angle = abs(calc_angle([1,0], max_direction));
    current_angle = max_angle;
    best_angle = 0;
    for i = 1:20
        [~, ~, ~, blocked] = trace_through_objective(p,...
            [cos(current_angle);sin(current_angle)], true);
        if ~blocked
            best_angle = current_angle;
            current_angle = current_angle + (1/2^i)*max_angle;
            if current_angle > max_angle
                break
            end
        else
            current_angle = current_angle - (1/2^i)*max_angle;
        end
    end

    % trace rays from scene into camera
    num_rays = 50;
    ray_points = cell(num_rays, 1);
    ray_directions = cell(num_rays, 1);
    for i = 1:num_rays
        current_angle = i/num_rays * best_angle;
        [x, y, v, blocked] = trace_through_objective(p,...
            [cos(current_angle);sin(current_angle)], true);
        if ~blocked
            % extend ray after last lens using the back focal point estimation 
            % from Smith's 'Modern Lens Design'
            y = [y; y(end)+(2*f_main-x(end))/v(1)*v(2)];
            x = [x; 2*f_main];
            % save rays
            ray_points{i} = [x,y];
            ray_directions{i} = v;
        end
    end

    % for debugging plot the traced rays
    if debug_visualization
        figure;
        title("Tracing from scene into camera for focus calculation");
        visualize_rays(ray_points);
        visualize_rays(ray_points, true);
    end

    % calculate focus point as mean of optical axis intersections of rays
    b_main = 0;
    for i = 1:num_rays
        m = (ray_points{i}(end, 2) - ray_points{i}(end-1, 2)) / ...
            (ray_points{i}(end, 1) - ray_points{i}(end-1, 1));
        b = ray_points{i}(end-1, 2) - (ray_points{i}(end-1, 1) * m);
        b_main = b_main + 1/num_rays * (- b / m);
    end
    
    d_main = temp_aperture;
    
end