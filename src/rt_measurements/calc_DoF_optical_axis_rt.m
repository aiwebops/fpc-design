%
% \brief calc_DoF_optical_axis_rt calculates the DoF for the sensor's
%                               center pixel
%
% \return DoF                	minimum and maximum distance to be in focus
% \return DoF_size            	size of the DoF
%
function [DoF, DoF_size] = calc_DoF_optical_axis_rt()

    global a_main b_main a_ML b_ML;
    global pixel_size;
    global d_ML;
    
    d_objective_sensor = b_main + a_ML + b_ML;

    a_main_min = a_main;
    for step_d = 0:10
        within_one_pixel = true;
        while within_one_pixel
            a_main_current = a_main_min - 32 * 0.5^step_d;

            max_angle = 0;
            for step_angle = 0:10
                hits_sensor = true;
                while hits_sensor && within_one_pixel
                    current_angle = max_angle + 0.5^step_angle * pi/180;
                    [x,y,w,blocked] = trace_through_objective(...
                        [-a_main_current; 0], ...
                        [cos(current_angle);sin(current_angle)], true);
                    if ~blocked
                        [x_MLA,y_MLA,w_MLA,blocked] = trace_through_two_plane_MLA(...
                            [x(end);y(end)], w, true, 2);
                        
                        if 2 * abs(y_MLA(2)) > d_ML
                            blocked = true;
                        end
                        
                        if ~blocked
                            % calculate sensor hit by solving p+sv = 
                            % (d_objective_sensor,*)
                            s = (d_objective_sensor - x_MLA(end)) / w_MLA(1);
                            sensor_hit = y_MLA(2) + s * w_MLA(2);
                            within_one_pixel = (2*abs(sensor_hit)<pixel_size);
                            max_angle = current_angle;
                        end   
                    end
                    hits_sensor = ~blocked;
                end
                
                if ~within_one_pixel
                    break
                end
                
            end
            if within_one_pixel
                a_main_min = a_main_current;
            end
        end
    end

    a_main_max = a_main;
    for step_d = 0:10
        within_one_pixel = true;
        while within_one_pixel
            a_main_current = a_main_max + 32 * 0.5^step_d;

            max_angle = 0;
            for step_angle = 0:10
                hits_sensor = true;
                while hits_sensor && within_one_pixel
                    current_angle = max_angle + 0.5^step_angle * pi/180;
                    [x,y,w,blocked] = trace_through_objective(...
                        [-a_main_current; 0], ...
                        [cos(current_angle);sin(current_angle)], true);
                    if ~blocked
                        [x_MLA,y_MLA,w,blocked] = trace_through_two_plane_MLA(...
                            [x(end);y(end)], w, true, 2);
                        
                        if 2 * abs(y_MLA(2)) > d_ML
                            blocked = true;
                        end
                        
                        if ~blocked
                            % calculate sensor hit by solving p+sv = 
                            % (d_objective_sensor,*)
                            s = (d_objective_sensor - x_MLA(end)) / w(1);
                            sensor_hit = y_MLA(2) + s * w(2);
                            within_one_pixel = (2*abs(sensor_hit)<pixel_size);
                            max_angle = current_angle;
                        end   
                    end
                    hits_sensor = ~blocked;
                end
                
                if ~within_one_pixel
                    break
                end
            end
            
            if within_one_pixel
                a_main_max = a_main_current;
                
                if a_main_max - a_main > ...
                        10 * a_main_min - a_main
                    a_main_max = inf;
                    break
                end
                
            end
            
        end
        
        if a_main_max == inf
            break
        end
    end

    DoF = [a_main_min; a_main_max];
    DoF_size = a_main_max - a_main_min;
end