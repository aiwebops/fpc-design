%
% \brief calc_ML_focus_point_min_blur_rt calculates the minimum blur 
%                               spot for the sensor position
%
% \param ray_points             open aperture traced rays points
% \param ray_directions         open aperture traced rays positions
%
% \return                       minimum blur approximation            
%
function focus_ML_min_blur = calc_ML_focus_point_min_blur_rt(...
    ray_points, ray_directions)

    global d_ML;

    % trace rays from scene into camera
    num_rays = size(ray_points,1);
    focus_rays = cell(num_rays, 1);
    focus_directions = cell(num_rays, 1);
    num_sensor_rays = 0;
    for i = 1:num_rays
        [x, y, v, blocked] = trace_through_two_plane_MLA(... 
            ray_points{i}(end,:), ray_directions{i}, true, 2);
        if ~blocked && (2*abs(y(end)) < d_ML)
            % save rays
            focus_rays{num_sensor_rays + 1} = [x,y];
            focus_directions{num_sensor_rays + 1} = v;
            num_sensor_rays = num_sensor_rays + 1;
        end
    end

    % calculate line representations
    m = zeros(num_sensor_rays, 1);
    b = zeros(num_sensor_rays, 1);
    for i = 1:num_sensor_rays
        if focus_directions{i}(1) == 0
            m(i) = 0;
            b(i) = focus_rays{i}(end,1);
        else
            m(i) = focus_directions{i}(2) / focus_directions{i}(1);
            b(i) = focus_rays{i}(end, 2) - (focus_rays{i}(end, 1) * m(i));
        end
    end
    
    % calculate focus point as minimum ray envelope cross section, i.e. the
    % position of minimum blur radius
    min_ray_idx = 1;
    max_ray_idx = 1;
    % next intersection positions of max and min ray
    next_x_max = inf;
    next_x_min = inf;
    % calculate next intersection of min ray - this is the first ray
    % crossing the optical axis and will be used as starting point
    for i = 1:num_sensor_rays
        if i ~= min_ray_idx
            x_min = (b(min_ray_idx) - b(i)) / (m(i) - m(min_ray_idx));
            if x_min < next_x_min
                next_x_min = x_min;
                next_min_idx = i;
            end
        end
    end
    current_x = x_min;
    
    % find initial ray with maximum height
    max_y = 0;
    for i = 2:num_sensor_rays
        y = m(i) * current_x + b(i);
        if y > max_y
            max_ray_idx = i;
            max_y = y;
        end
    end
    
    % the following loop will jump to the next intersection position of
    % either max or min array depending on which is closer to the current
    % position
    
    % true if position moved to next max intersection in last iteration
    next_to_calc_is_max = true;
    % smallest blur radius given by the ray envelope cross section
    smallest_section = inf;
    
    decreasing = true;
    while decreasing
        % calculate next max ray intersection
        if next_to_calc_is_max
            for i = 1:num_sensor_rays
                if i ~= max_ray_idx
                    x_max = (b(max_ray_idx) - b(i)) / (m(i) - m(max_ray_idx));
                    if x_max > current_x && x_max < next_x_max
                        next_x_max = x_max;
                        next_max_idx = i;
                    end
                end
            end
        % calculate next min ray intersection
        else
            for i = 1:num_sensor_rays
                if i ~= min_ray_idx
                    x_min = (b(min_ray_idx) - b(i)) / (m(i) - m(min_ray_idx));
                    if x_min > current_x && x_min < next_x_min
                        next_x_min = x_min;
                        next_min_idx = i;
                    end
                end
            end
        end
        
        % check, which intersection is closest to the current position
        if next_x_max < next_x_min
            % go on with max step
            current_x = next_x_max;
            next_x_max = inf;
            max_ray_idx = next_max_idx;
            next_to_calc_is_max = true;
        else
            % go on with min step
            current_x = next_x_min;
            next_x_min = inf;
            min_ray_idx = next_min_idx;
            next_to_calc_is_max = false;
        end
        
        % calculate ray envelope cross section
        section = max(abs((m(max_ray_idx) * current_x + b(max_ray_idx))), ...
             abs(m(min_ray_idx) * current_x + b(min_ray_idx)));
        
        % check whether the current blur radius is smaller than the
        % previous one
        decreasing = (section < smallest_section);
        if decreasing
            smallest_section = section;
            focus_ML_min_blur = current_x;
        end
    end
end