%
% \brief auto_design_full_DoF basically puts the example rt_DoF.m into a 
% single function to simplify its use.
%
function auto_design_full_DoF(DoF_, f_ML_, d_ML_, lambda_, pixel_size_,...
    sensor_height_, lensfile_path, save_file)

    clearvars -except DoF_ f_ML_ d_ML_ lambda_ pixel_size_ ...
        sensor_height_ lensfile_path save_file lenses_path lensfiles;

    %%%%%%%%%%%%%%%%%%%%%%%% Load objective data %%%%%%%%%%%%%%%%%%%%%%%%%%
    
    data;
    read_lensfile(lensfile_path);

    %%%%%%%%%%%% Set desired parameters for thin lens estimation %%%%%%%%%%

    DoF = DoF_;
    f_ML = f_ML_;
    d_ML = d_ML_; % will be optimized
    lambda = lambda_;

    pixel_size = pixel_size_;
    sensor_height = sensor_height_;

    %%%%%%%%%%%%%%%%%%%% Calculate remaining parameters %%%%%%%%%%%%%%%%%%%

    optimization_rt_DoF();

    %%%%%%%%%%%%%%%%%%%% Calculate DoF for center pixel %%%%%%%%%%%%%%%%%%%

    %[DoF, DoF_size] = calc_DoF_optical_axis_rt();

    %%%%%%%%%%%%%%%%%%%%%% Save config and clean up %%%%%%%%%%%%%%%%%%%%%%%
    [~, lensfile, file_ending] = fileparts(lensfile_path);
    save_blender_config(strcat(lensfile,file_ending), save_file);

end