clear all

image_dir = '../data/renderings/';
image_list = dir(image_dir);

setup_names = {'1','2','3','4','5','6','7','8','9','10','11','12',...
    '13','14','15','16','17','18','19','20','21','22','23','24','25'};
method_names = {'thin_lens','lensmaker','rt'};
resolution_names = {'0'};%,'1'};
white_image_suffix = 'white';
num_imgs_per_set = 20;

num_curves = size(setup_names, 2) * size(method_names, 2) * ...
    size(resolution_names, 2);

contrast_data = zeros(2 * num_curves, 20);
counter = -1;

for setup_idx = 1:size(setup_names, 2)
    for resolution_idx = 1:size(resolution_names, 2)
        for method_idx = 1:size(method_names, 2)
            
            counter = counter + 2;
            % load white image
            file_name_begin = append(setup_names{setup_idx}, '_', ...
                method_names{method_idx}, '_');
            file_name_end = append('_', ...
                resolution_names{resolution_idx},'.png');
            white_image_name = append(file_name_begin, ...
                white_image_suffix, file_name_end);
            white_image = imread(append(image_dir, white_image_name));
            
            [img_width, img_height, ~] = size(white_image);
            cutout_size = round(img_width * 8 / 45); %empirical value for cutout
            min_px = img_width/2 - (cutout_size/2 - 1);
            max_px = round(min_px + cutout_size - 1);
            min_px = round(min_px);
            
            white_image = white_image(min_px:max_px, min_px:max_px, 1);
            white_image = double(max(1, white_image)); % prevent div by zero
            
            filelist = image_list(startsWith({image_list.name}, ...
                file_name_begin));
            filelist = filelist(endsWith({filelist.name}, file_name_end));
            filelist = natsortfiles(filelist);
            for img_idx = 1:size(filelist, 1)-1 %last is white image
                
                line_width = filelist(img_idx).name(...
                    size(file_name_begin,2)+1:...
                    size(filelist(img_idx).name,2)-size(file_name_end,2));
                contrast_data(counter,img_idx) = str2double(line_width);
                
                % load image
                current_image = imread(append(image_dir, ...
                    filelist(img_idx).name));
                
                % get center region
                current_image = double(current_image(min_px:max_px, ...
                    min_px:max_px, 1));
                
                % normalize
                current_image = current_image ./ white_image;
                
                % calculate mean of dark and bright values
                mean_value = mean(current_image);
                max_mean = mean(current_image(current_image > mean_value));
                min_mean = mean(current_image(current_image < mean_value));
                
                % calculate and save contrast
                if isnan(max_mean) || isnan(min_mean)
                    contrast = 0;
                else
                    contrast = max_mean - min_mean;
                end
                
                contrast_data(counter+1,img_idx) = contrast;
            end
        end
    end
end

figure;
hold on;
plot_colors = jet(10);
for i = 2:25
    color = plot_colors(1, :);
    plot(1 / max(contrast_data(6*i - 1,:)) * contrast_data(6*i - 1,:),contrast_data(6*i,:), 'Color', color, 'LineWidth', 2);
end
for i = 1:25
    color = plot_colors(10, :);
    plot(1 / max(contrast_data(6*i - 3,:)) * contrast_data(6*i - 3,:),contrast_data(6*i-2,:), 'Color', color, 'LineWidth', 2);
end
% for i = 1:10
%     color = plot_colors(8, :);
%     plot(10 / max(contrast_data(6*i - 5,:)) * contrast_data(6*i - 5,:),contrast_data(6*i-4,:), 'Color', color, 'LineWidth', 2);
% end

figure;
hold on;
idx = 21;
color = plot_colors(1, :);
plot(contrast_data(6*idx - 1,:),contrast_data(6*idx,:), 'Color', color, 'LineWidth', 2);
color = plot_colors(10, :);
plot(contrast_data(6*idx - 3,:),contrast_data(6*idx-2,:), 'Color', color, 'LineWidth', 2);
color = plot_colors(8, :);
plot(contrast_data(6*idx - 5,:),contrast_data(6*idx-4,:), 'Color', color, 'LineWidth', 2);
xlabel('Line Width in mm') 
ylabel('Contrast') 
legend('Proposed','Thick Lens','Thin Lens');

% calculate normalized mean, min and max curves for contrast
mean_rt = sum(contrast_data(6:6:end,:))/25;
min_rt = min(contrast_data(6:6:end,:), [], 1);
max_rt = max(contrast_data(6:6:end,:), [], 1);

mean_lm = sum(contrast_data(4:6:end,:))/25;
min_lm = min(contrast_data(4:6:end,:), [], 1);
max_lm = max(contrast_data(4:6:end,:), [], 1);

mean_tl = sum(contrast_data(2:6:end,:))/25;
min_tl = min(contrast_data(2:6:end,:), [], 1);
max_tl = max(contrast_data(2:6:end,:), [], 1);

centered_contrast = contrast_data;
for i = 1:25
    centered_contrast(6*i,:) = centered_contrast(6*i,:) - mean_rt;
    centered_contrast(4 + 6*(i-1),:) = centered_contrast(4 + 6*(i-1),:) - mean_lm;
    centered_contrast(2 + 6*(i-1),:) = centered_contrast(2 + 6*(i-1),:) - mean_tl;
end
centered_contr_2 = centered_contrast;
centered_contrast = centered_contrast .*centered_contrast;
variance_rt = sum(centered_contrast(6:6:end,:))/25;
variance_lm = sum(centered_contrast(4:6:end-2,:))/25;
variance_tl = sum(centered_contrast(2:6:end-4,:))/25;

figure;
hold on;
x=linspace(0,1,20);
color = plot_colors(1, :);
errorbar(x, mean_rt, variance_rt, 'Color', color, 'LineWidth', 2);
%errorbar(x, mean_rt(end:-1:1), variance_rt(end:-1:1), 'Color', color, 'LineWidth', 2);
plot(x,mean_rt, 'Color', color, 'LineWidth', 2);
 plot(x,min_rt, 'Color', color, 'LineWidth', 2);
 plot(x,max_rt, 'Color', color, 'LineWidth', 2);

color = plot_colors(10, :);
plot(x,mean_lm, 'Color', color, 'LineWidth', 2);
 plot(x,min_lm, 'Color', color, 'LineWidth', 2);
 plot(x,max_lm, 'Color', color, 'LineWidth', 2);
%errorbar(x, mean_lm(end:-1:1), variance_lm(end:-1:1), 'Color', color, 'LineWidth', 2);
errorbar(x, mean_lm, variance_lm, 'Color', color, 'LineWidth', 2);

legend('Ray Tracing','Thick Lens')

%color = plot_colors(4, :);
%plot(x,mean_tl, 'Color', color, 'LineWidth', 2);
% plot(x,min_tl, 'Color', color, 'LineWidth', 2);
% plot(x,max_tl, 'Color', color, 'LineWidth', 2);
%errorbar(x, mean_tl, variance_tl, 'Color', color, 'LineWidth', 2);

clearvars -except contrast_data