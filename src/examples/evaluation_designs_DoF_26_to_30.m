clear all

% Path for resulting configs - make sure, the folder exists!
config_dir = './';
% Path to lens config folder
lenses_path = '../lens_data/';
% Lens files
lensfiles = {'D-Gauss6 F1.4 34.4deg_Merte-Zeiss DRP485798-1927 p306.csv', ...
    'D-Gauss7 F1.4 46deg_Yoshisato Fujioka USP4443070 p336.csv', ...
    'D-Gauss8 F1.1 30.2deg_Angeniux USP2701987 p347.csv', ...
    'Fisheye F2 94deg_Kikuo Momiyam USP 4437735 p160.csv', ...
    'Fisheye F8 170.8deg_Miyamoto Josa 1964 p166.csv'};

data;

% mainlens idx, DoF min, DoF max, f_ML, d_ML (will be optimized), lambda, 
% pixel_size, sensor_height
test_parameters = {
    [1, 900, 1100, 0.8, 0.3, 0.25, 0.01, 30], ...
    [2, 1500, 2000, 1.5, 0.3, 0.2, 0.01, 30], ...
    [3, 500, 530, 1, 0.2, 0.35, 0.01, 30], ...
    [4, 950, 1150, 4, 0.7, 0.3, 0.03, 90], ...
    [5, 1500, 1750, 3, 0.5, 0.35, 0.02, 120]};

num_setups = size(test_parameters, 2);

for i = 1:num_setups

    par = test_parameters{i};
    setup_name = int2str(25 + i); % the first 25 are for the refinement 
                                  % evaluation
    setup_name = strcat(config_dir, setup_name);
    auto_design_full_DoF([par(2), par(3)], par(4), par(5), par(6), ...
        par(7), par(8), strcat(lenses_path,lensfiles{1}), ...
        strcat(setup_name,'_dof.csv'));
end
fprintf("Done.\n");