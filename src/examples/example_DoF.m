% Lens file
lensfile = '../lens_data/D-Gauss7 F1.4 46deg_Yoshisato Fujioka USP4443070 p336.csv';
% Save file
savefile = 'result.csv';

% Desired parameters
DoF = [500,550];
f_ML = 1.5;
d_ML = 0.3; % will be optimized, but needs initial value
lambda = 0.3;

pixel_size = 0.01;
sensor_height = 30;

% Call optimization
auto_design_full_DoF(DoF, f_ML, d_ML, lambda, pixel_size,...
    sensor_height, lensfile, savefile);