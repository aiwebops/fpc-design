%
% \brief calc_b_ML_thin_lens calculates the distance between sensor and MLA 
%                   according to the thin lens model
%
% \param a_ML       distance between virtual image and MLA in mm
% \param b_main     distance between main lens and image side focus
%                   point in mm
% \param lambda 	disparity coefficient
%
% \return           distance between sensor and MLA in mm
%
function b_ML = calc_b_ML_thin_lens(a_ML, b_main, lambda)
    b_ML = (lambda * a_ML * (b_main+a_ML)) / ...
        ((b_main+a_ML) - a_ML * (1+lambda));
end