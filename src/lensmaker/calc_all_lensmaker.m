%
% \brief calc_all_lensmaker calculates the camera parameters according 
%                   to the thin lens model for the MLA and thick lens for
%                   the main lens
%
% \param a_main     distance between main lens scene point in mm
% \param f_main     focal length of the main lens in mm
% \param f_ML       focal length of the microlens in mm
% \param d_ML       microlens diameter in mm
% \param lambda     disparity coefficient
%
% \return b_main    distance between image side focus point and main lens
%                   in mm
% \return d_main    main lens aperture size in mm
% \return a_ML      distance between virtual image and MLA in mm
% \return b_ML      distance between sensor and MLA in mm
%
function [b_main, d_main, a_ML, b_ML] = calc_all_lensmaker(a_main, ...
    f_main, f_ML, d_ML, lambda)

    [p_1,p_2] = calc_principal_planes();
    b_main = calc_b_main_thick_lens(p_1, p_2, f_main, a_main);
    a_ML = calc_a_ML_thin_lens(f_ML, b_main, lambda);
    b_ML = calc_b_ML_thin_lens(a_ML, b_main, lambda);
    d_main = calc_d_main_lensmaker(b_main, a_ML, b_ML, d_ML);
    
end