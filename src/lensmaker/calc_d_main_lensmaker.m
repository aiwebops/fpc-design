%
% \brief calc_d_main_lensmaker calculates the aperture size of the main 
%                   lens based on the magnification and a single traced ray
%
% \param b_main     distance between main lens and image side focus
%                   point in mm
% \param a_ML       distance between virtual image and MLA in mm
% \param b_ML    	distance between sensor and MLA in mm
% \param d_ML       microlens diameter in mm
%
% \return           main lens aperture size in mm
%
function d_main = calc_d_main_lensmaker(b_main, a_ML, b_ML, d_ML)

    global num_lens_surfaces aperture_index;
    num_lenses = num_lens_surfaces - aperture_index;

    % init ray starting on the sensor
    pixel = [b_main + a_ML + b_ML; d_ML /2];
    direction = [-b_ML; -pixel(2)]; 
    [x, y, v, ~] = trace_through_objective(pixel, direction, false);
    
    % if ray is blocked before reaching the aperture, decrease the pixel
    % distance to the sensor center
    while size(x, 1) < num_lenses
        pixel = pixel - [0;0.01];
        direction = [-b_ML; -pixel(2)]; 
        [x, y, v, ~] = trace_through_objective(pixel, direction, ...
            false);
    end
    
    % calculate height of ray at aperture position
    if size(x, 1) == num_lenses
        d_main = 2*abs(y(num_lenses) + x(num_lenses) * ...
            (y(num_lenses+1) - y(num_lenses)) / ...
            (x(num_lenses+1) - x(num_lenses)));
    else
        d_main = 2*abs(y(num_lenses) + x(num_lenses) * v(2) / v(1));
    end
end