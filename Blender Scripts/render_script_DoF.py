import bpy
import os
from os.path import isfile, join
from Blender_CamGen import io, data

############################################## Configuration ##############################################

# Set the dir, in which the renderings should be stored 
output_path = 'SET PATH HERE!'
# Set the dir containing the camera configurations created with the Matlab scripts
configurations_path = 'SET PATH HERE!'
# Set the target DoF ranges for all configurations. In this example, we had 5 configs (26-30). In the 
# rendering loop, the stripe pattern will then be placed at 100 different positions between 0.75*DoF_min 
# and 1.25*DoF_max for each camera
DoF_mins = [900,1500,500,950,1500]
DoF_maxs = [1100,2000,530,1150,1750]

###########################################################################################################

# float to string conversion functions
def STRf(value):
    return "{:.4f}".format(value)
def STRi(value):
    return "{:.0f}".format(value)


scene = bpy.data.scenes['Scene']

# set image properties
scene.cycles.samples = 1024
scene.render.image_settings.file_format='PNG'
scene.render.image_settings.color_mode='RGB'
scene.render.image_settings.color_depth='16'

# get config files
config_files = [f for f in os.listdir(configurations_path) if isfile(join(configurations_path, f))]
config_files=sorted(config_files)

for config_idx in range(0,len(config_files)):
    
    config = config_files[config_idx]
    DoF_min = 0.75*DoF_mins[config_idx]
    DoF_max = 1.25*DoF_maxs[config_idx]

    ####################### Load plenoptic camera config ####################

    cg = bpy.data.scenes[0].camera_generator
    filepath = configurations_path+'/'+config
    filename = os.path.splitext(config)[0]
    # read the camera parameters from csv file
    io.read_cam_params(filepath)
    # create camera using the GUI setup
    data.use_gui_data = True
    bpy.ops.camgen.createcam()
    data.use_gui_data = False

    ######################## Create calibration pattern ######################

    bpy.ops.camgen.createcalibrationpattern()
    mat_tree = bpy.data.materials['Calibration Pattern Material'].node_tree

    ########### Render image section for different numbers of lines #########

    bpy.context.scene.render.border_min_x = 0.5 - 0.5*cg.prop_microlens_diam/(1000*cg.prop_sensor_width)
    bpy.context.scene.render.border_min_y = bpy.context.scene.render.border_min_x
    bpy.context.scene.render.border_max_x = 1 - bpy.context.scene.render.border_min_x
    bpy.context.scene.render.border_max_y = bpy.context.scene.render.border_max_x
    scene.render.use_border=True
    scene.render.use_crop_to_border=True

    # render white image
    mat_tree.links.remove(mat_tree.nodes['Lines'].outputs['Color'].links[0])
    scene.cycles.seed = 42
    white_image_file_name = filename+'_white.png'
    scene.render.filepath = output_path+white_image_file_name
    bpy.ops.render.render(write_still=True)
    
    mat_tree.links.new(mat_tree.nodes['Siemens Star'].outputs['Brightness'],mat_tree.nodes['Emission'].inputs[0])
    
    mat_tree.nodes['Siemens Star'].inputs[2].default_value=1
    mat_tree.nodes['Siemens Star'].inputs[2].default_value=16
    
    # render stripe images
    scene.cycles.seed = 0
    for i in range(0,99):
        pattern_position = DoF_min + (i/99) * (DoF_max -DoF_min) 
        bpy.data.objects['Calibration Pattern'].location[0] = -pattern_position/1000
        image_file_name = filename+'_'+STRf(pattern_position)+'.png'
        scene.render.filepath = output_path + image_file_name
        bpy.ops.render.render(write_still=True)
