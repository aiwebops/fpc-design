import bpy
import os
from os.path import isfile, join
from Blender_CamGen import io, data

############################################## Configuration ##############################################

# Set the dir, in which the renderings should be stored 
output_path = 'SET PATH HERE!'
# Set the dir containing the camera configurations created with the Matlab scripts
configurations_path = 'SET PATH HERE!'
# Set a factor for the stripe frequency on the calibration pattern. Due to the different main lenses, we 
# had to use different frequency ranges. This factor was set to 1.5 for setups 1-5, 1.1 for 6-10, 3 for 
# 11-15, 2.5 for 16-20, 1.8 for 21-25. These are empirical values tuned in order to have 3 visible stripes 
# in the center microlens at 100mm focus distance.
stripe_frequency_factor = 1.1

###########################################################################################################


# float to string conversion functions
def STRf(value):
    return "{:.4f}".format(value)
def STRi(value):
    return "{:.0f}".format(value)


scene = bpy.data.scenes['Scene']

# set image properties
scene.cycles.samples = 1024
scene.render.image_settings.file_format='PNG'
scene.render.image_settings.color_mode='RGB'
scene.render.image_settings.color_depth='16'

# get config files
config_files = [f for f in os.listdir(configurations_path) if isfile(join(configurations_path, f))]

for config in config_files:

    ####################### Load plenoptic camera config ####################

    cg = bpy.data.scenes[0].camera_generator
    filepath = configurations_path+'/'+config
    filename = os.path.splitext(config)[0]
    # read the camera parameters from csv file
    io.read_cam_params(filepath)
    # create camera using the GUI setup
    data.use_gui_data = True
    bpy.ops.camgen.createcam()
    data.use_gui_data = False

    ######################## Create calibration pattern #####################

    bpy.ops.camgen.createcalibrationpattern()
    mat_tree = bpy.data.materials['Calibration Pattern Material'].node_tree

    ########### Render image section for different numbers of lines #########

    bpy.context.scene.render.border_min_x = 0.5 - 1.5 * cg.prop_microlens_diam/(1000*cg.prop_sensor_width)
    bpy.context.scene.render.border_min_y = bpy.context.scene.render.border_min_x
    bpy.context.scene.render.border_max_x = 1 - bpy.context.scene.render.border_min_x
    bpy.context.scene.render.border_max_y = bpy.context.scene.render.border_max_x
    scene.render.use_border=True
    scene.render.use_crop_to_border=True

    max_line_size = cg.prop_focus_distance / 100 * stripe_frequency_factor
    orig_pixel_size = cg.prop_pixel_size

    for resolution_step in range(0,2):
        # render in different resolutions
        cg.prop_pixel_size = orig_pixel_size * pow(0.5, resolution_step)
        # render white image
        mat_tree.links.remove(mat_tree.nodes['Lines'].outputs['Color'].links[0])
        scene.cycles.seed = 42
        white_image_file_name = filename+'_white_'+STRi(resolution_step)+'.png'
        scene.render.filepath = output_path+white_image_file_name
        bpy.ops.render.render(write_still=True)
        mat_tree.links.new(mat_tree.nodes['Lines'].outputs['Color'],mat_tree.nodes['Emission'].inputs[0])
        
        # render stripe images
        scene.cycles.seed = 0
        for i in range(0,20):
            current_line_size = i/19 * max_line_size
            bpy.data.materials['Calibration Pattern Material'].node_tree.nodes['Lines'].inputs[0].default_value = current_line_size
            image_file_name = filename+'_'+STRf(current_line_size)+'_'+STRi(resolution_step)+'.png'
            scene.render.filepath = output_path + image_file_name
            bpy.ops.render.render(write_still=True)
            
    cg.prop_pixel_size = orig_pixel_size
